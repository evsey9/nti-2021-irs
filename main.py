import sys
import time

from robot_library.robot import Robot  # pylint: disable=no-name-in-module disable=import-error
import cv2
import rospy
import numpy as np
import math


# Module imports
from modules import helpers
from modules import mathtools
from modules import colortools
from modules import matrixtools
from modules import artag

from modules import code_globals
from modules import robot_globals


# Global variables
pi = 3.141592653589793
DEBUG = True

camera_x_size = 160
camera_y_size = 120


robot_dimensions = {  # All in meters
	'width': 0.381,
	'length': 0.442,
	'wheel_diameter': 0.18,
	'track': 0.3
}

robot_const = {
	'cpr': math.pi * 2,
	'to_wall': 25,
	'v': 0.7,
	'turn_v': 1,
	'v_min': 0.05,  
	'turn_v_min': 0.2,
	'max_v': 2,
	'max_turn_v': 2
}

code_globals.DEBUG = DEBUG

robot_globals.robot_const = robot_const
robot_globals.robot_dimensions = robot_dimensions
robot = Robot()
robot_globals.robot_object = robot

robot_var = {
	'azimuth': helpers.Dir.UP,
	'x': 0,
	'y': 0,
	'cell': 0
}
robot_globals.robot_var = robot_var

base_angles = [180, -90, 0, 90]  # Robot yaw is 0 degrees when robot is looking down

robot_globals.angles = base_angles


cell_size = 0.6
maze_width = 8
maze_height = 8

robot_globals.consts = {
	"cell_size": cell_size,
	"maze_width": maze_width,
	"maze_height": maze_height,
}

# Must import after definition of global variables (robot modules) :
from modules import robot_movement as movement  
from modules import robot_laser as laser
from modules import robot_slam as slam


class Program:
	"""Contains main code"""
	def exec_init(self):
		__interpretation_started_timestamp__ = time.time()
		__gyro_started_timestamp__ = time.time()
		robot_globals.__interpretation_started_timestamp__ = __interpretation_started_timestamp__
		robot_globals.__gyro_started_timestamp__ = __gyro_started_timestamp__
		robot_var['azimuth'] = slam.get_robot_azimuth()
		return
	
	def exec_main(self):
		
		return
		
	def exec_program(self):
		self.exec_init()
		self.exec_main()
		return


def main():
	program = Program()
	program.exec_program()


if __name__ == '__main__':
	main()
