import unittest
import json
import pathlib
import modules.matrixtools as matrixtools

TEST_FILE = pathlib.Path(__file__).parent.joinpath("test_matrixtools.json")


class TestMatrixtoolsFunctions(unittest.TestCase):
	with open(TEST_FILE) as json_file:
		test_data = json.load(json_file)

	def test_list_to_matrix(self):
		current_test_data = self.test_data["test_list_to_matrix"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = matrixtools.list_to_matrix(test_input[0], test_input[1])
				self.assertEqual(result, expected_output)

	def test_matrix_to_list(self):
		current_test_data = self.test_data["test_matrix_to_list"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = list(matrixtools.matrix_to_list(test_input))  # Result before conversion is itertools.chain obj
				self.assertEqual(result, expected_output)

	def test_rotate_matrix_90_degrees_clockwise(self):
		current_test_data = self.test_data["test_rotate_matrix_90_degrees_clockwise"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = list(matrixtools.rotate_matrix_90_degrees_clockwise(test_input))
				self.assertEqual(result, expected_output)

	def test_get_90_deg_cw_rotated_matrix_index(self):
		current_test_data = self.test_data["test_get_90_deg_cw_rotated_matrix_index"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = list(matrixtools.get_90_deg_cw_rotated_matrix_index((len(test_input[0]), len(test_input[0][0])), test_input[1]))
				self.assertEqual(result, expected_output)

	def test_matrix_map(self):  # No JSON for this test
		with self.subTest("Checking matrix_map without additional arguments"):
			test_input = [[1, 1], [1, 1]]
			expected_output = [[2, 2], [2, 2]]
			result = matrixtools.matrix_map(lambda x: x + 1, test_input)
			self.assertEqual(result, expected_output)
		with self.subTest("Checking matrix_map with an additional argument"):
			test_input = [[1, 1], [1, 1]]
			expected_output = [[3, 3], [3, 3]]
			result = matrixtools.matrix_map(lambda x, y: x + y, test_input, 2)
			self.assertEqual(result, expected_output)
		with self.subTest("Checking matrix_map with 2 additional arguments"):
			test_input = [[1, 1], [1, 1]]
			expected_output = [[6, 6], [6, 6]]
			result = matrixtools.matrix_map(lambda x, y, z: x + y + z, test_input, 2, 3)
			self.assertEqual(result, expected_output)
		with self.subTest("Checking matrix_map with argument matrix"):
			test_input = [[1, 1], [1, 1]]
			expected_output = [[4, 8], [12, 16]]
			argument_matrix = [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]
			result = matrixtools.matrix_map(lambda x, *y: x + sum(y), test_input, argument_matrix, arg_matrix=True)
			self.assertEqual(result, expected_output)
		with self.subTest("Checking matrix_map with argument matrix and additional argument"):
			test_input = [[1, 1], [1, 1]]
			expected_output = [[5, 9], [13, 17]]
			argument_matrix = [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]
			result = matrixtools.matrix_map(lambda x, *y: x + sum(y), test_input, argument_matrix, 1, arg_matrix=True)
			self.assertEqual(result, expected_output)


if __name__ == '__main__':
	unittest.main()
