import unittest
import json
import pathlib
import modules.artag as artag

TEST_FILE = pathlib.Path(__file__).parent.joinpath("test_artag.json")


class TestARTag(unittest.TestCase):
	with open(TEST_FILE) as json_file:
		test_data = json.load(json_file)

	def test_artag_init(self):
		current_test_data = self.test_data["test_artag_init"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest(test_input=test_input, expected_output=expected_output):
				new_artag = artag.ARTag(test_input)
				result = new_artag.arcells
				self.assertEqual(result, expected_output)

	def test_find_corner_straight(self):
		current_test_data = self.test_data["test_find_corner_straight"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest(test_input=test_input, expected_output=expected_output):
				new_artag = artag.ARTag(8)
				result = new_artag.find_corner_straight(test_input[0], test_input[1])
				result = result.to_list("x y") if type(result) != bool else result
				self.assertEqual(result, expected_output)

	def test_find_corner_diagonal(self):
		current_test_data = self.test_data["test_find_corner_diagonal"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest(test_input=test_input, expected_output=expected_output):
				new_artag = artag.ARTag(8)
				result = new_artag.find_corner_diagonal(test_input[0], test_input[1])
				result = result.to_list("x y") if type(result) != bool else result
				self.assertEqual(result, expected_output)

	def test_find_corners_with_single_function(self):
		current_test_data = self.test_data["test_find_corners_with_single_function"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest(test_input=test_input, expected_output=expected_output):
				new_artag = artag.ARTag(8)
				corner_functions = [new_artag.find_corner_straight, new_artag.find_corner_diagonal]
				results = []
				for i, func_i in enumerate(corner_functions):
					result_i = new_artag.find_corners_with_single_function(test_input[0], func_i, test_input[1])
					result_i = list(map(lambda vec: vec.to_list("x y"), result_i)) if type(result_i) != bool else result_i
					results.append(result_i)
				self.assertEqual(results, expected_output)


if __name__ == '__main__':
	unittest.main()
