import unittest
import json
import pathlib
import modules.mathtools as mathtools

TEST_FILE = pathlib.Path(__file__).parent.joinpath("test_mathtools.json")


class TestMathtoolsFunctions(unittest.TestCase):
	with open(TEST_FILE) as json_file:
		test_data = json.load(json_file)

	def test_sign(self):
		current_test_data = self.test_data["test_sign"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = mathtools.sign(test_input)
				self.assertEqual(result, expected_output)

	def test_intersect_lines(self):
		current_test_data = self.test_data["test_intersect_lines"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = list(mathtools.intersect_lines(*test_input))
				self.assertAlmostEqual(result[0], expected_output[0], 3)
				self.assertAlmostEqual(result[1], expected_output[1], 3)

	def test_quadrilateral_area(self):
		current_test_data = self.test_data["test_quadrilateral_area"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = mathtools.quadrilateral_area(*test_input)
				self.assertEqual(result, expected_output)

	def test_map_value_to_range(self):
		current_test_data = self.test_data["test_map_value_to_range"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = mathtools.map_value_to_range(*test_input)
				self.assertEqual(result, expected_output)


if __name__ == '__main__':
	unittest.main()
