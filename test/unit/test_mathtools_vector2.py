import unittest
import json
import pathlib
from modules.mathtools import Vector2

TEST_FILE = pathlib.Path(__file__).parent.joinpath("test_mathtools_vector2.json")


class TestVector2(unittest.TestCase):
	with open(TEST_FILE) as json_file:
		test_data = json.load(json_file)

	def test_init(self):
		current_test_data = self.test_data["test_init"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				vector = Vector2(*test_input)
				result = [vector.x, vector.y]
				self.assertEqual(result, expected_output)

	def test_add(self):
		current_test_data = self.test_data["test_add"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = Vector2(*test_input[0])
				for vector_i in test_input[1:]:
					act_vector_i = Vector2(*vector_i)
					result += act_vector_i
				self.assertEqual(result, Vector2(*expected_output))

	def test_sub(self):
		current_test_data = self.test_data["test_sub"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = Vector2(*test_input[0])
				for vector_i in test_input[1:]:
					act_vector_i = Vector2(*vector_i)
					result -= act_vector_i
				result = [result.x, result.y]
				self.assertAlmostEqual(result[0], expected_output[0], 5)  # Floating point math.
				self.assertAlmostEqual(result[1], expected_output[1], 5)

	def test_mul(self):
		current_test_data = self.test_data["test_mul"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = Vector2(*test_input[0]) * test_input[1]
				result = [result.x, result.y]
				self.assertAlmostEqual(result[0], expected_output[0], 5)  # Floating point math.
				self.assertAlmostEqual(result[1], expected_output[1], 5)

	def test_div(self):
		current_test_data = self.test_data["test_div"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = Vector2(*test_input[0]) / test_input[1]
				result = [result.x, result.y]
				self.assertAlmostEqual(result[0], expected_output[0], 5)  # Floating point math.
				self.assertAlmostEqual(result[1], expected_output[1], 5)

	def test_length(self):
		current_test_data = self.test_data["test_length"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = Vector2(*test_input).length()
				self.assertAlmostEqual(result, expected_output, 3)  # Floating point math.

	def test_bool(self):
		current_test_data = self.test_data["test_bool"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = bool(Vector2(*test_input))
				self.assertEqual(result, expected_output)

	def test_str(self):
		current_test_data = self.test_data["test_str"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = str(Vector2(*test_input))
				self.assertEqual(result, expected_output)

	def test_to_list(self):
		current_test_data = self.test_data["test_to_list"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = Vector2(*test_input[0]).to_list(test_input[1])
				self.assertEqual(result, expected_output)

	def test_normalized(self):
		current_test_data = self.test_data["test_normalized"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = Vector2(*test_input).normalized()
				result = [result.x, result.y]
				self.assertAlmostEqual(result[0], expected_output[0], 2)  # Floating point math.
				self.assertAlmostEqual(result[1], expected_output[1], 2)

	def test_dot(self):
		current_test_data = self.test_data["test_dot"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = Vector2(*test_input[0]).dot(Vector2(*test_input[1]))
				self.assertAlmostEqual(result, expected_output, 3)  # Floating point math.


if __name__ == '__main__':
	unittest.main()
