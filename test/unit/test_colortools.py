import unittest
import json
import pathlib
import modules.colortools as colortools
TEST_FILE = pathlib.Path(__file__).parent.joinpath("test_colortools.json")


class TestColortoolsFunctions(unittest.TestCase):
	with open(TEST_FILE) as json_file:
		test_data = json.load(json_file)

	def test_rgb_hex_array_to_rgb_24(self):
		current_test_data = self.test_data["test_rgb_hex_array_to_rgb_24"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = colortools.rgb_hex_array_to_rgb_24(test_input)
				self.assertEqual(result, expected_output)

	def test_rgb24_to_rgb(self):
		current_test_data = self.test_data["test_rgb24_to_rgb"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = colortools.rgb24_to_rgb(test_input)
				self.assertEqual(result, tuple(expected_output))  # No support for tuples in JSON, so we convert it

	def test_rgb_to_greyscale(self):
		current_test_data = self.test_data["test_rgb_to_greyscale"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = colortools.rgb_to_greyscale(test_input)
				self.assertAlmostEqual(result, expected_output, 5)  # Use AlmostEqual because of float math

	def test_greyscale_to_binary(self):
		current_test_data = self.test_data["test_greyscale_to_binary"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("Checking if result of input p1 equals output p2", p1=test_input, p2=expected_output):
				result = colortools.greyscale_to_binary(test_input[0], test_input[1])  # Input is list of two arguments
				self.assertEqual(result, expected_output)


if __name__ == '__main__':
	unittest.main()
