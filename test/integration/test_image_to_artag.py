import unittest
import json
import pathlib

from modules import artag
TEST_FILE = pathlib.Path(__file__).parent.joinpath("test_image_to_artag.json")


class TestImageToARTag(unittest.TestCase):
	with open(TEST_FILE) as json_file:
		test_data = json.load(json_file)

	def test_hex_list_to_artag_values(self):
		current_test_data = self.test_data["test_hex_list_to_artag_values"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				new_artag = artag.ARTag(8)
				artag_input = test_input.split()
				result = new_artag.hex_list_to_artag_values(artag_input, 160, 120)
				self.assertEqual(result, expected_output)


if __name__ == '__main__':
	unittest.main()
