import unittest
import json
import pathlib
TEST_FILE = pathlib.Path(__file__).parent.joinpath("$test_file")


class MyTestCase(unittest.TestCase):
	with open(TEST_FILE) as json_file:
		test_data = json.load(json_file)

	def test_something(self):
		current_test_data = self.test_data["test_something"]
		for test_input, expected_output in zip(current_test_data["input"], current_test_data["output"]):
			with self.subTest("input=p1, expected output=p2", p1=test_input, p2=expected_output):
				result = "something"
				self.assertEqual(result, expected_output)


if __name__ == '__main__':
	unittest.main()
