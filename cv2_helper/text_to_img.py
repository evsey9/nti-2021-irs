import cv2
import numpy as np
import json

import sys
import os
sys.path.append(
	os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from modules import colortools
from modules import matrixtools
from modules import mathtools
from modules import artag

x_size = 160
y_size = 120

delta = 5

ar_cells = 6

out_file = "last_image_read.json"

def main():
	# Converting HEX string to ARTag
	img = cv2.imread("/home/evsey/nti-2021-irs/artag_img.png")
	print("image base shape:", img.shape)
	new_artag = artag.ARTag()
	preprocessed_image = new_artag.preprocess_image(img)
	print("preprocessed shape:", preprocessed_image.shape)
	rgb_matrix = cv2.cvtColor(preprocessed_image, cv2.COLOR_BGR2RGB)
	hsv_matrix = cv2.cvtColor(rgb_matrix, cv2.COLOR_RGB2HSV_FULL)
	grey_matrix = cv2.cvtColor(rgb_matrix, cv2.COLOR_RGB2GRAY)
	mask_matrix = colortools.grey_matrix_to_mask_matrix(grey_matrix)

	left_lines_mask_matrix = new_artag.filter_left_lines(hsv_matrix)
	right_lines_mask_matrix = new_artag.filter_right_lines(hsv_matrix)

	corners_straight = new_artag.find_corners_with_single_function(mask_matrix, new_artag.find_corner_straight)
	corners_diagonal = new_artag.find_corners_with_single_function(mask_matrix, new_artag.find_corner_diagonal)
	if corners_diagonal:
		corners_straight_list = sum(list(map(lambda x: x.to_list("x y"), corners_straight)), [])
		corners_diagonal_list = sum(list(map(lambda x: x.to_list("x y"), corners_diagonal)), [])
		area_straight = mathtools.quadrilateral_area(*corners_straight_list)
		area_diagonal = mathtools.quadrilateral_area(*corners_diagonal_list)
		print("area straight:", area_straight, "area diagonal:", area_diagonal)
		if area_straight > area_diagonal:
			corners_correct = corners_straight
			print("picked straight corners")
		else:
			corners_correct = corners_diagonal
			print("picked diagonal corners")
		artag_matrix = new_artag.get_artag_matrix_from_corners(mask_matrix, corners_correct)
		print("artag matrix:", artag_matrix)
		oriented_artag_matrix = new_artag.get_oriented_artag_matrix(artag_matrix)
		if oriented_artag_matrix:
			print("oriented artag matrix:", oriented_artag_matrix)
			artag_bits = new_artag.get_artag_bits(artag_matrix)
			print("artag bits:", artag_bits)
			hamming_decoded_bits = new_artag.hamming_decode(artag_bits)
			print("hamming decoded bits:", hamming_decoded_bits)
			if type(hamming_decoded_bits) == int:  # hamming decode function returned number, must mean there are too many errors in the ARTag
				print("more than 1 error in bits")
				print("turn direction", new_artag.get_turn_direction(left_lines_mask_matrix, right_lines_mask_matrix))
			else:
				artag_value = new_artag.parse_bits(hamming_decoded_bits)
				print("artag value:", artag_value)

		else:
			print("did not find artag, no orientation bit")
			print("turn direction", new_artag.get_turn_direction(left_lines_mask_matrix, right_lines_mask_matrix))
	else:
		print("did not find artag, no corners")
		print("turn direction", new_artag.get_turn_direction(left_lines_mask_matrix, right_lines_mask_matrix))
	np_bgr_matrix = preprocessed_image
	np_grey_matrix = grey_matrix
	np_mask_matrix = mask_matrix * 255

	np_artag_matrix = cv2.cvtColor(np_mask_matrix, cv2.COLOR_GRAY2BGR)

	np_left_lines_mask_matrix = cv2.cvtColor(left_lines_mask_matrix * 255, cv2.COLOR_GRAY2BGR)
	np_right_lines_mask_matrix = cv2.cvtColor(right_lines_mask_matrix * 255, cv2.COLOR_GRAY2BGR)
	np_lines_combined_mask_matrix = np_left_lines_mask_matrix + np_right_lines_mask_matrix
	np_left_lines_color_matrix = np.array(np_left_lines_mask_matrix * (0, 1, 0), dtype=np.uint8)  # matrix becomes dtype=int64 after multiplication
	np_right_lines_color_matrix = np.array(np_right_lines_mask_matrix * (1, 0, 0), dtype=np.uint8)
	np_lines_combined_color_matrix = np_left_lines_color_matrix + np_right_lines_color_matrix

	np_artag_matrix = np_artag_matrix - np_lines_combined_mask_matrix + np_lines_combined_color_matrix
	if corners_diagonal:
		for i in range(1, 4):
			cv2.line(np_artag_matrix, tuple(corners_correct[i - 1].to_list("x y")), tuple(corners_correct[i].to_list("x y")), (0, 0, 255), 1)
		cv2.line(np_artag_matrix, tuple(corners_correct[3].to_list("x y")), tuple(corners_correct[0].to_list("x y")), (0, 0, 255), 1)

	cv2.imshow("raw", img)
	cv2.imshow("preprocessed", np_bgr_matrix)
	cv2.imshow("grey", np_grey_matrix)
	cv2.imshow("mask", np_mask_matrix)
	cv2.imshow("artag", np_artag_matrix)
	cv2.imshow("lines", np_lines_combined_color_matrix)
	cv2.waitKey()


if __name__ == "__main__":
	main()
