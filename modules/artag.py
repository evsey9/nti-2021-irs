import numpy as np
import cv2

import modules.matrixtools as matrixtools
import modules.mathtools as mathtools
import modules.colortools as colortools
# ARTag related code


class ARTag:
	"""Contains ARTag detector and data
	"""
	arcells = 6
	def __init__(self):
		pass

	def find_corner_straight(self, mask_matrix, delta=0):
		"""Finds the top left corner of ARTag ( the first black pixel ) by scanning horizontally from the top left
		:param list[list[int]] mask_matrix:
		:param int delta:
		:return: Vector2 position of corner
		"""
		x, y = delta, delta
		while y < len(mask_matrix) - delta and mask_matrix[y][x]:
			if x < len(mask_matrix[0]) - 1 - delta:
				x += 1
			else:
				x = delta
				y += 1
		if y < len(mask_matrix) - delta:  # Making sure that we actually found a corner
			return mathtools.Vector2(x, y)
		else:
			return False

	def find_corner_diagonal(self, mask_matrix, delta=0):
		"""Finds the top left corner of ARTag ( the first black pixel ) by scanning diagonally-down from the top left
		:param list[list[int]] mask_matrix:
		:param int delta:
		:return: Vector2 position of corner
		"""
		x, y, n = delta, delta, 0
		while n < len(mask_matrix[0]) + len(mask_matrix) - delta * 4 - 1 and mask_matrix[y][x]:
			if y < len(mask_matrix) - 1 - delta and x > delta:
				y += 1
				x -= 1
			else:
				n += 1
				y = max(delta, n - len(mask_matrix[0]) + delta * 3 + 1)
				x = min(delta + n, len(mask_matrix[0]) - 1 - delta)

		if n < len(mask_matrix[0]) + len(mask_matrix) - delta * 4 - 1:
			return mathtools.Vector2(x, y)
		else:
			return False

	def find_corners_with_single_function(self, mask_matrix, corner_function, delta=0):
		"""
		Finds corners of of ARTag mask_matrix using given corner function
		:param list[list[int]] mask_matrix: 2D list of 1s and 0s, where 1 is white and 0 is black
		:param corner_function: Function that returns top left corner of a mask matrix
		:param int delta: How many pixels from the sides to ignore. Has to be positive
		:return: 4 corner Vector2s if all corners are found, otherwise false. [top_l, bottom_l, bottom_r, top_r]
		"""
		corners = []
		current_matrix = mask_matrix.copy()
		base_shape = (len(current_matrix), len(current_matrix[0]))
		current_shape = base_shape
		for i in range(4):  # 4 corners
			corner_i = corner_function(current_matrix, delta=delta)
			corrected_corner = corner_i
			if type(corner_i) == bool and not corner_i:
				return False  # If corner == False it means corner was not found, therefore we can stop searching
			temp_shape = current_shape
			for j in range((4 - i) % 4):
				corrected_corner = matrixtools.get_90_deg_cw_rotated_matrix_index(temp_shape, corrected_corner.to_list("y x"))
				corrected_corner = mathtools.Vector2(*corrected_corner[::-1])
				temp_shape = temp_shape[::-1]
			corners.append(corrected_corner)
			current_matrix = matrixtools.rotate_matrix_90_degrees_clockwise(current_matrix)
			current_shape = current_shape[::-1]
		return corners

	def get_artag_matrix_from_corners(self, mask_matrix, corners):
		"""Gets a matrix of inner ARTag cells given 4 ARTag corners
		:param list[list[int]] mask_matrix: Binary matrix of 1s and 0s
		:param list[mathtools.Vector2] corners: 4 corners of ARTag in mask_matrix [top_left, bottom_left, bottom_right, top_right]
		:return: Unoriented ARTag cell matrix (square matrix of dimensions self.arcells - 2)
		"""
		top_left_corner = corners[0]
		bottom_left_corner = corners[1]
		bottom_right_corner = corners[2]
		top_right_corner = corners[3]

		centre = (corners[0] + corners[1] + corners[2] + corners[3]) / 4  # Unused. Maybe remove?

		side_vectors = {
			"top": top_right_corner - top_left_corner,
			"left": bottom_left_corner - top_left_corner,
			"bottom": bottom_right_corner - bottom_left_corner,
			"right": bottom_right_corner - top_right_corner
		}
		unit_vectors = {}
		cell_vectors = {}
		for key in side_vectors.keys():
			unit_vectors[key] = side_vectors[key].normalized()
			cell_vectors[key] = unit_vectors[key] * side_vectors[key].length() / self.arcells

		artag_matrix = [[1 for j in range(self.arcells - 2)]
							for i in range(self.arcells - 2)]  # ARTag has a border of black cells, we only need the inner part

		for y_cell in range(1, self.arcells - 1):
			for x_cell in range(1, self.arcells - 1):
				centre_top = top_left_corner + cell_vectors["top"] * (x_cell + 0.5)  # Add 0.5 so that the lines meet at the centre
				centre_bottom = bottom_left_corner + cell_vectors["bottom"] * (x_cell + 0.5)
				centre_left = top_left_corner + cell_vectors["left"] * (y_cell + 0.5)
				centre_right = top_right_corner + cell_vectors["right"] * (y_cell + 0.5)

				# Determine cell by getting intersection of horizontal and vertical line that meet at the centre of the cell
				intersection_point = mathtools.Vector2(*mathtools.intersect_lines(centre_top.x, centre_top.y,
																					centre_bottom.x, centre_bottom.y,
																					centre_left.x, centre_left.y,
																					centre_right.x, centre_right.y))
				artag_matrix[y_cell - 1][x_cell - 1] = mask_matrix[round(intersection_point.y)][round(intersection_point.x)]

		return artag_matrix

	def get_oriented_artag_matrix(self, artag_matrix):
		"""Gets an ARTag cell matrix that is oriented in such a way that the orientation bit is in the bottom right corner
		:param list[list] artag_matrix: Base unoriented ARTag cell matrix
		:return: Oriented ARTag cell matrix
		"""
		if artag_matrix[0][0]:  # Orientation bit is at the top left, rotate 180 degrees
			corrected_artag_matrix = matrixtools.rotate_matrix_90_degrees_clockwise(artag_matrix)
			corrected_artag_matrix = matrixtools.rotate_matrix_90_degrees_clockwise(corrected_artag_matrix)
		elif artag_matrix[self.arcells - 3][0]:  # Orientation bit is at the bottom left, rotate 270 degrees
			corrected_artag_matrix = matrixtools.rotate_matrix_90_degrees_clockwise(artag_matrix)
			corrected_artag_matrix = matrixtools.rotate_matrix_90_degrees_clockwise(corrected_artag_matrix)
			corrected_artag_matrix = matrixtools.rotate_matrix_90_degrees_clockwise(corrected_artag_matrix)
		elif artag_matrix[0][self.arcells - 3]:  # Orientation bit is at the top right, rotate 90 degrees
			corrected_artag_matrix = matrixtools.rotate_matrix_90_degrees_clockwise(artag_matrix)
		elif artag_matrix[self.arcells - 3][self.arcells - 3]:  # Orientation bit as at the bottom right, already oriented
			corrected_artag_matrix = artag_matrix
		else:  # All orientation bits are black, something must've went wrong
			return False
		return corrected_artag_matrix

	def get_artag_bits(self, artag_matrix):
		"""Gets the bits of an ARTag cell matrix
		:param list[list[int]] artag_matrix: ARTag cell matrix
		:return: List of bits of size (self.arcells - 2) ** 2 - 4
		"""
		bits = []
		for i in range(self.arcells - 2):
			for j in range(self.arcells - 2):
				if i == 0 or i == self.arcells - 3:  # We don't take corner cells into consideration
					if j == 0 or j == self.arcells - 3:
						continue
				bits.append(int(not artag_matrix[i][j]))
		return bits

	def hamming_decode(self, hamming_bits):
		"""Uses Hamming Decode to do Hamming Decoding with control bits that can fix one error in the bits
		:param list[int] hamming_bits: Bits that have control bits
		:return: Proper bits without the control bits, with the improper bit fixed if there was one
		"""
		hamming_bits = hamming_bits.copy()
		arr_out = []
		arr_test = []
		k = 1
		for i, elem in enumerate(hamming_bits):
			arr_out.append(elem)
			if i == k - 1:
				k *= 2
			else:
				arr_test.append(elem)

		i = 1
		while i <= len(arr_out):
			k = 0
			rrr = [[i], []]
			i1 = i
			while i1 < len(hamming_bits):
				i2 = i1
				while i2 < i1 + i and i2 <= len(hamming_bits):
					if i2 != i:
						k += hamming_bits[i2 - 1]
					i2 += 1
				i1 += i * 2
			arr_out[i - 1] = k % 2
			i *= 2

		k = 0
		for i, elem in enumerate(arr_out):
			if hamming_bits[i] != elem:
				k += i + 1

		k = k - 1 if k != 0 else k
		if k > len(hamming_bits):
			return k

		hamming_bits[k] = (hamming_bits[k] + 1) % 2
		arr_out = []
		k = 1
		for i, elem in enumerate(hamming_bits):
			if i == k - 1:
				k *= 2
			else:
				arr_out.append(elem)

		return arr_out

	def parse_bits(self, bits):
		"""Parses bits into a value
		:param list[int] bits: Decoded bits of the ARTag
		:return: Tuple of 2 numbers
		"""
		bits = list(map(str, bits.copy()))
		num1 = int(''.join(bits[:3][::-1]), 2)
		num2 = int(''.join(bits[3:6][::-1]), 2)
		return num1, num2

	def filter_left_lines(self, matrix):
		"""Returns a mask matrix of HSV colors that fit within the green left line color boundaries
		:param matrix: HSV color Matrix to filter lines in
		:return: 2D matrix of 1s and 0s
		"""
		left_line_color_min = (55, 25, 25)
		left_line_color_max = (140, 255, 179)
		return cv2.inRange(matrix, left_line_color_min, left_line_color_max) // 255

	def filter_right_lines(self, matrix):
		"""Returns a mask matrix of HSV colors that fit within the blue right line color boundaries
		:param matrix: HSV color Matrix to filter lines in
		:return: 2D matrix of 1s and 0s
		"""
		right_line_color_min = (130, 15, 40)
		right_line_color_max = (180, 150, 150)
		return cv2.inRange(matrix, right_line_color_min, right_line_color_max) // 255

	def get_turn_direction(self, left_lines_mask_matrix, right_lines_mask_matrix):
		"""Checks whether the robot needs to turn left or right to see the ARTag properly.
		:param left_lines_mask_matrix: Mask matrix of lines to the left of the ARTag
		:param right_lines_mask_matrix: Mask matrix of lines to the right of the ARTag
		:return: String L if robot needs to turn left, string R if robot needs to turn right, false if robot doesn't need to turn/can't figure out where to turn
		"""
		has_left_lines = matrixtools.check_if_matrix_has_true_element(left_lines_mask_matrix)
		has_right_lines = matrixtools.check_if_matrix_has_true_element(right_lines_mask_matrix)
		if has_left_lines ^ has_right_lines:
			if has_left_lines:
				return "R"
			else:
				return "L"
		else:
			return False

	def preprocess_image(self, img):
		"""Applies the necessary preprocessing onto an image

		:param img: CV2 image
		"""
		up_delta = 0
		left_delta = 0
		right_delta = 0
		down_delta = 45
		img_width = 640
		img_height = 360
		img = cv2.resize(img, (img_width, img_height))
		img = img[up_delta:img.shape[0] - down_delta, left_delta:img.shape[1] - right_delta]
		img = cv2.fastNlMeansDenoisingColored(img, None, 10, 10, 7, 21)
		return img

	def read_artag(self, img, debug=False):
		"""Turns a CV2 image (BGR format) into a list of decoded ARTag values
		:param img: CV2 image
		:param bool debug: Whether to display debug data or not
		:return: List of parsed ARTag values
		"""
		preprocessed_image = self.preprocess_image(img)
		rgb_matrix = cv2.cvtColor(preprocessed_image, cv2.COLOR_BGR2RGB)
		hsv_matrix = cv2.cvtColor(rgb_matrix, cv2.COLOR_RGB2HSV_FULL)
		grey_matrix = cv2.cvtColor(rgb_matrix, cv2.COLOR_RGB2GRAY)
		mask_matrix = colortools.grey_matrix_to_mask_matrix(grey_matrix)

		left_lines_mask_matrix = self.filter_left_lines(hsv_matrix)
		right_lines_mask_matrix = self.filter_right_lines(hsv_matrix)

		corners_straight = self.find_corners_with_single_function(mask_matrix, self.find_corner_straight)
		corners_diagonal = self.find_corners_with_single_function(mask_matrix, self.find_corner_diagonal)

		if corners_straight is False or corners_diagonal is False:  # no corners found
			return self.get_turn_direction(left_lines_mask_matrix, right_lines_mask_matrix)

		corners_straight_list = sum(list(map(lambda x: x.to_list("x y"), corners_straight)), [])
		corners_diagonal_list = sum(list(map(lambda x: x.to_list("x y"), corners_diagonal)), [])
		area_straight = mathtools.quadrilateral_area(*corners_straight_list)
		area_diagonal = mathtools.quadrilateral_area(*corners_diagonal_list)
		if area_straight > area_diagonal:
			corners_correct = corners_straight
			if debug:
				print("picked straight corners")
		else:
			corners_correct = corners_diagonal
			if debug:
				print("picked diagonal corners")
		artag_matrix = self.get_artag_matrix_from_corners(mask_matrix, corners_correct)
		oriented_artag_matrix = self.get_oriented_artag_matrix(artag_matrix)
		if oriented_artag_matrix is False:  # no orientation bit in matrix, ARTag detected improperly
			return self.get_turn_direction(left_lines_mask_matrix, right_lines_mask_matrix)
		artag_bits = self.get_artag_bits(artag_matrix)
		hamming_decoded_bits = self.hamming_decode(artag_bits)
		if type(hamming_decoded_bits) == int:
			return self.get_turn_direction(left_lines_mask_matrix, right_lines_mask_matrix)  # hamming decode function returned number, must mean there are too many errors in the ARTag
		artag_value = self.parse_bits(hamming_decoded_bits)
		if debug:
			print("area straight:", area_straight, "area diagonal:", area_diagonal)
			print("artag matrix:", artag_matrix)
			print("oriented artag matrix:", oriented_artag_matrix)
			print("artag bits:", artag_bits)
			print("artag value:", artag_value)

		return artag_value

