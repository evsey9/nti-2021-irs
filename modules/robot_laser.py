import modules.robot_general as robot_general
import modules.robot_globals as robot_globals
import modules.code_globals as code_globals
import modules.helpers as helpers
import modules.mathtools as mathtools
import math

from modules.robot_globals import robot_object as robot

class LaserReading:
	"""Class for laser readings of the robot
	"""
	first_invalid_readings = 30  # How many first readings hit the robot itself 30
	last_invalid_readings = 30  # How many last readings hit the robot itself 30
	def __init__(self, laser_readings_dict=None):
		"""Initializes laser readings from a laser readings dictioanry

		:param dict laser_readings_dict: Dictionary containing angle increment, total laser angle, laser values and timestamp
		"""
		if laser_readings_dict is None:
			laser_readings_dict = robot.getLaser()
		
		self.laser_readings_dict = laser_readings_dict  # Robot laser readings dictionary
		self.angle_increment = self.laser_readings_dict['angle_increment']  # Angle increment between each laser reading in radians
		self.laser_angle = self.laser_readings_dict['angle']  # The coverage angle of all the laser readings
		self.valid_laser_angle = self.laser_angle - self.first_invalid_readings * self.angle_increment - self.last_invalid_readings * self.angle_increment  # The coverage angle of all the valid laser readings
		self.laser_values = self.laser_readings_dict['values'][::-1]  # List of all the laser readings
		self.valid_laser_values = self.laser_values[self.first_invalid_readings:len(self.laser_values) - self.last_invalid_readings]  # List of all the valid laser readings. The laser readings are reversed, so we reverse the first/last invalid positions
		self.timestamp = self.laser_readings_dict['time_stamp']  # Timestamp of the laser readings
		
	def get_index_from_angle(self, angle, degrees=True):
		"""Gets the index of the laser reading corresponding to the angle that is closest to the given angle

		:param float angle: Angle of the laser reading
		:param bool degrees: Whether the angle is in degrees or not
		"""
		if degrees:
			angle = math.radians(angle)
		angle += self.laser_angle / 2
		# if angle < (self.laser_angle - self.valid_laser_angle) / 2 or angle > self.valid_laser_angle + (self.laser_angle - self.valid_laser_angle) / 2:
		#     raise ValueError("Angle outside of laser bounds")
		
		index = round(angle / self.angle_increment)
		if index < self.first_invalid_readings or index >= len(self.laser_values) - self.last_invalid_readings:
			raise ValueError("Angle outside of laser bounds")

		return index

	def get_reading_from_angle(self, *angles, degrees=True):
		"""Gets the laser reading from the angle closest to the given angle

		:param angle: Angle or list of angles of the laser readings
		:param bool degrees: Whether the angle is in degrees or not
		"""
		values = []
		for i_angle in angles:
			reading_index = self.get_index_from_angle(i_angle, degrees)
			values.append(self.laser_values[reading_index])
		return values[0] if len(angles) == 1 else values
	
	def get_reading_range(self, left_boundary_angle=None, right_boundary_angle=None, degrees=True):
		"""Gets a range of readings from angle to angle

		:param left_boundary_angle: Left angle boundary
		:param right_boundary_angle: Right angle boundary
		:param degrees: Whether the angles are in degrees or not
		:return: List of laser readings
		"""
		left_boundary_index = self.first_invalid_readings if left_boundary_angle is None else self.get_index_from_angle(left_boundary_angle)
		right_boundary_index = len(self.laser_values) - self.last_invalid_readings - 1 if right_boundary_angle is None else self.get_index_from_angle(right_boundary_angle)
		return self.laser_values[left_boundary_index:right_boundary_index + 1]
	
	def get_func_reading_from_range(self, left_boundary_angle=None, right_boundary_angle=None, func=min, degrees=True):
		"""Applies func to reading range (must return a single unchanged value!) and return the value and angle in degrees

		:param left_boundary_angle: Left angle boundary
		:param right_boundary_angle: Right angle boundary
		:param func: Function to apply on range
		:param degrees: Whether the angles are in degrees or not
		:return: Tuple of reading value and reading angle in degrees
		"""
		reading_range = self.get_reading_range(left_boundary_angle, right_boundary_angle, degrees)
		func_value = func(reading_range)
		value_index = reading_range.index(func_value)
		value_angle = self.angle_increment * value_index - self.laser_angle / 2
		return func_value, math.degrees(value_angle)

