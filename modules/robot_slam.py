import modules.robot_general as robot_general
import modules.robot_globals as robot_globals
import modules.code_globals as code_globals
import modules.helpers as helpers
import modules.mathtools as mathtools
import modules.robot_movement as movement
import modules.robot_laser as laser
import math
from collections import defaultdict
import numpy as np

from modules.robot_globals import robot_object as robot

# Functions for simultaneous localization and mapping (SLAM)

def get_free_distances():
	"""Gets the distances a robot can go to in each direction, with None meaning robot does not see in the direction
	"""
	laser_reading = laser.LaserReading()
	reading_arr = laser_reading.get_reading_from_angle(-90, 0, 90)
	free_distance_list = [None, None, None, None]
	for read_index, direction in enumerate([helpers.Dir.LEFT, helpers.Dir.UP, helpers.Dir.RIGHT]):
		free_distance_list[direction] = int(reading_arr[read_index] // robot_globals.consts["cell_size"])
	return free_distance_list


def get_free_directions():
	"""Gets what directions a robot can go in, with 0 meaning direction is blocked, 1 meaning direction is free, and None meaning that the sensors cannot check there
	"""
	free_distance_list = get_free_distances()
	for i, dist in enumerate(free_distance_list):
		if dist is not None:
			free_distance_list[i] = min(1, dist)
	return free_distance_list


def convert_local_directions_to_global(local_directions):
	"""Converts a list of free directions relative to the robot's rotation into a list of free directions relative to initial rotatiion

	:param local_directions: List of 4 objects, with each object corresponding to a diretion relative to robot
	"""
	return np.roll(local_directions, robot_globals.robot_var["azimuth"])


def get_global_free_directions():
	"""Combines get_free_directions() and convert_local_directions_to_global()
	"""
	directions = get_free_directions()
	global_directions = convert_local_directions_to_global(directions)
	return global_directions


def get_global_free_distances():
	"""Combines get_free_distances() and convert_local_directions_to_global()
	"""
	distances = get_free_distances()
	global_distances = convert_local_directions_to_global(distances)
	return global_distances


def get_robot_azimuth():
	"""Returns azimuth of the robot from current yaw
	"""
	current_yaw = movement.get_yaw()
	deltas = []
	for i in range(4):
		deltas.append(abs(movement.calculate_delta_angle(current_yaw, robot_globals.angles[i])))

	min_delta = min(deltas)
	min_index = deltas.index(min_delta)
	return min_index


def graphs_to_action_path(start_direction, graphs):
	"""Turns a list of cells into a list of actions for the robot to take to move along those cells

	:param start_direction: Starting direction of the robot
	:param graphs: List of cells in path
	:return: List of actions (F for go forward, L for turn left, R for turn right)
	"""
	actpath = []
	curdir = start_direction
	for i in range(1, len(graphs)):
		curcell = graphs[i]
		pathdelta = [graphs[i][0] - graphs[i - 1][0], graphs[i][1] - graphs[i - 1][1]]
		if pathdelta == helpers.DirVec.LEFT:  # Going left
			if curdir == helpers.Dir.LEFT:
				actpath.append("F")
			elif curdir == helpers.Dir.UP:
				actpath.append("L")
				actpath.append("F")
			elif curdir == helpers.Dir.RIGHT:
				actpath.append("L")
				actpath.append("L")
				actpath.append("F")
			elif curdir == helpers.Dir.DOWN:
				actpath.append("R")
				actpath.append("F")
			curdir = helpers.Dir.LEFT
		elif pathdelta == helpers.DirVec.UP:  # Going up
			if curdir == helpers.Dir.UP:
				actpath.append("F")
			elif curdir == helpers.Dir.RIGHT:
				actpath.append("L")
				actpath.append("F")
			elif curdir == helpers.Dir.DOWN:
				actpath.append("L")
				actpath.append("L")
				actpath.append("F")
			elif curdir == helpers.Dir.LEFT:
				actpath.append("R")
				actpath.append("F")
			curdir = helpers.Dir.UP
		if pathdelta == helpers.DirVec.RIGHT:  # Going right
			if curdir == helpers.Dir.RIGHT:
				actpath.append("F")
			elif curdir == helpers.Dir.DOWN:
				actpath.append("L")
				actpath.append("F")
			elif curdir == helpers.Dir.LEFT:
				actpath.append("L")
				actpath.append("L")
				actpath.append("F")
			elif curdir == helpers.Dir.UP:
				actpath.append("R")
				actpath.append("F")
			curdir = helpers.Dir.RIGHT
		if pathdelta == helpers.DirVec.DOWN:  # Going down
			if curdir == helpers.Dir.DOWN:
				actpath.append("F")
			elif curdir == helpers.Dir.LEFT:
				actpath.append("L")
				actpath.append("F")
			elif curdir == helpers.Dir.UP:
				actpath.append("L")
				actpath.append("L")
				actpath.append("F")
			elif curdir == helpers.Dir.RIGHT:
				actpath.append("R")
				actpath.append("F")
			curdir = helpers.Dir.DOWN
	return actpath


class Map():
	"""Class for simultaneous localization and mapping (SLAM) in a labyrinth
	"""
	def __init__(self):
		self.explored_cells = [[0, 0]]
		self.seen_unexplored_cells = []
		self.maze_map = np.full((robot_globals.robot_var["y"] + 1, robot_globals.robot_var["x"] + 1, 4), None)
		self.displacement_vector = [0, 0]
	
	def update_map(self, readings=None, coords_base=None, explored=True, displacement_vector=None):
		"""Updates the maze map with readings from the current cell and updates robot coordinates

		:param readings: List of global directions the robot can go to from current cell (example: [0, 1, None, 1], where 0 - can't go, 1 - can go, and None is undefined). If None, then it uses current robot readings
		:param coords_base: Coordinates of robot reading. If None, then it uses current robot position
		:param explored: Whether to set the cell as explored or not
		"""
		if readings is None:
			readings = get_global_free_distances()
		if coords_base is None:
			coords = [robot_globals.robot_var["x"], robot_globals.robot_var["y"]]
		else:
			coords = coords_base.copy()
		if displacement_vector is None:
			displacement_vector = self.displacement_vector
		if displacement_vector is not self.displacement_vector:
			coords[0] += self.displacement_vector[0] - displacement_vector[0]
			coords[1] += self.displacement_vector[1] - displacement_vector[1]
			displacement_vector = self.displacement_vector
		if coords[0] >= self.maze_map.shape[1] or coords[0] < 0:
			delta = coords[0] - self.maze_map.shape[1] + 1 if coords[0] >= 0 else abs(coords[0])
			base = self.maze_map
			appended = np.full((self.maze_map.shape[0], delta, 4), None)
			if coords[0] < 0:
				base, appended = appended, base
				for cell in self.explored_cells:
					cell[0] += delta
				for cell in self.seen_unexplored_cells:
					cell[0] += delta
				coords[0] += delta
				robot_globals.robot_var["x"] += delta
				self.displacement_vector[0] += delta
			self.maze_map = np.hstack((base, appended))
		if coords[1] >= self.maze_map.shape[0] or coords[1] < 0:
			delta = coords[1] - self.maze_map.shape[0] + 1 if coords[1] >= 0 else abs(coords[1])
			base = self.maze_map
			appended = np.full((delta, self.maze_map.shape[1], 4), None)
			if coords[1] < 0:
				base, appended = appended, base
				for cell in self.explored_cells:
					cell[1] += delta
				for cell in self.seen_unexplored_cells:
					cell[1] += delta
				coords[1] += delta
				robot_globals.robot_var["y"] += delta
				self.displacement_vector[1] += delta
			self.maze_map = np.vstack((base, appended))
		if coords_base is None:
			robot_globals.robot_var["x"], robot_globals.robot_var["y"] = coords

		for i, elem in enumerate(readings):
			if elem is not None:
				self.maze_map[coords[1]][coords[0]][i] = elem
		if explored:
			if coords not in self.explored_cells:
				self.explored_cells.append(coords)
			if coords in self.seen_unexplored_cells:
				self.seen_unexplored_cells.remove(coords)
		if explored:
			for i, dir_free in enumerate(readings):
				if dir_free:
					new_coords = [robot_globals.robot_var["x"] + helpers.DirVec.vectors[i][0], robot_globals.robot_var["y"] + helpers.DirVec.vectors[i][1]]
					displacement_vector_copy = displacement_vector.copy()
					if new_coords not in self.explored_cells and new_coords not in self.seen_unexplored_cells:
						self.seen_unexplored_cells.append(new_coords)
						dirs = [None] * 4
						dirs[(i + 2) % 4] = 1
						self.update_map(dirs, new_coords, False, displacement_vector_copy)
		
	def map_to_graph(self):
		"""Turns the maze map matrix into a graph object

		:return: Graph object of the explored maze
		:rtype: Graph
		"""
		map_graph = Graph(self.maze_map.shape[0] * self.maze_map.shape[1], robot_globals.robot_var["azimuth"])
		for y in range(self.maze_map.shape[0]):
			for x in range(self.maze_map.shape[1]):
				for i, elem in enumerate(self.maze_map[y][x]):
					if elem:
						dir_vec = helpers.DirVec.vectors[i]
						new_coord = (x + dir_vec[0], y + dir_vec[1])
						map_graph.add_edge((x, y), new_coord)
		return map_graph
	
	def get_action_path_to_cell(self, end_cell):
		"""Return shortest action path from current robot location to end_cell coordinate

		:param end_cell: Coordinate of robot destination
		:return: List of actions (F for go forward, L for turn left, R for turn right)
		"""
		if type(end_cell) == list:
			end_cell = tuple(end_cell)
		robot_coord = (robot_globals.robot_var["x"], robot_globals.robot_var["y"])
		map_graph = self.map_to_graph()
		action_path = map_graph.get_shortest_path(robot_coord, end_cell, robot_globals.robot_var["azimuth"])
		return action_path

	def move_to_coordinate(self, end_cell):
		"""Moves robot to cell

		:param coord: Cell coordinate (can be explored or unexplored)
		"""
		if type(end_cell) is list:
			end_cell = tuple(end_cell)
		test_path = self.get_action_path_to_cell(end_cell)
		if test_path:
			movement.go_actions(test_path)
		else:
			while (robot_globals.robot_var["x"], robot_globals.robot_var["y"]) != end_cell:
				self.update_map()
				if None in self.maze_map[robot_globals.robot_var["y"]][robot_globals.robot_var["x"]]:
					movement.place_turn_right()
					self.update_map()
				min_coord = (0, 0)
				min_manh_dist = robot_globals.consts["maze_width"] + robot_globals.consts["maze_height"]
				for coord in self.seen_unexplored_cells:
					manh_dist = abs(end_cell[0] - coord[0]) + abs(end_cell[1] - coord[1])
					if manh_dist <= min_manh_dist:
						min_coord = coord
						min_manh_dist = manh_dist
				self.move_to_coordinate(min_coord)

	def localize(self):
		self.update_map()
		if None in self.maze_map[robot_globals.robot_var["y"]][robot_globals.robot_var["x"]]:
			movement.place_turn_right()
			self.update_map()
		while self.maze_map.shape != (robot_globals.consts["maze_height"], robot_globals.consts["maze_width"], 4):

			robot_coords = [robot_globals.robot_var["x"], robot_globals.robot_var["y"]]
			dir_max = None
			max_dist = 0
			for i, dist in enumerate(self.maze_map[robot_globals.robot_var["y"]][robot_globals.robot_var["x"]]):
				new_coords = [robot_coords[0] + helpers.DirVec.vectors[i][0], robot_coords[1] + helpers.DirVec.vectors[i][1]]
				if dist > max_dist and new_coords not in self.explored_cells:
					max_dist = dist
					dir_max = i
			if dir_max is not None:
				movement.place_turn_to(dir_max)
				movement.move_cells(max_dist, cell_func=self.update_map)
			else:
				min_coord = (0, 0)
				min_act_dist = robot_globals.consts["maze_width"] * robot_globals.consts["maze_height"] * 2
				min_path = []
				# print("cells:", self.seen_unexplored_cells)
				for cell_coord in self.seen_unexplored_cells:
					act_path = self.get_action_path_to_cell(cell_coord)
					# print("path", act_path)
					if len(act_path) <= min_act_dist:
						min_coord = cell_coord
						min_act_dist = len(act_path)
						min_path = act_path
				movement.go_actions(min_path, cell_func=self.update_map)	


class Graph:
	def __init__(self, vertices, start_dir):
		# No. of vertices
		self.V = vertices
		self.paths = []
		# default dictionary to store graph
		self.graph = defaultdict(list)
		self.shortest_actual_action_path = None
		self.start_dir = None

	# function to add an edge to graph
	def add_edge(self, u, v):
		self.graph[u].append(v)

	def bfs(self, start, end):
		if type(start) is list:
			start = tuple(start)
		if type(end) is list:
			end = tuple(end)
		queue = [start]
		visited = defaultdict(int)
		is_visited = defaultdict(int)
		parent = defaultdict(set)
		visited[start] = 1
		while len(queue) > 0:
			p = queue.pop(0)
			is_visited[p] = 1
			if p == end:
				break
			for v in self.graph[p]:
				if is_visited[v] == 0:
					visited[v] = visited[p] + 1
					queue.append(v)
					parent[v].add(p)
				elif v != start and visited[p] == visited[list(parent[v])[0]]:
					parent[v].add(p)
		for i in parent.keys():
			parent[i] = list(parent[i])
		paths = []

		def parents_to_paths(node, parents, cur_path=None):
			if cur_path is None:
				cur_path = [node]

			while len(parents[node]) == 1:
				cur_path.append(parents[node][0])
				node = parents[node][0]
			if len(parents[node]) == 0:
				paths.append(list(reversed(cur_path)))
			else:
				for next_node in parents[node]:
					parents_to_paths(next_node, parents, cur_path + [next_node])

		parents_to_paths(end, parent)
		return paths

	def get_shortest_path(self, start, end, start_dir):
		"""Return shortest action path from start to end with start_dir azimuth at the start

		:param start: Coordinate of start
		:param end: Coordinate of end
		:param start_dir: Starting azimuth
		:return: List of actions (F for go forward, L for turn left, R for turn right)
		"""
		self.start_dir = start_dir
		bfs_paths = self.bfs(start, end)
		bfs_action_paths = sorted(map(lambda x: graphs_to_action_path(start_dir, x), bfs_paths), key=lambda x: len(x))
		self.shortest_actual_action_path = bfs_action_paths[0]
		return self.shortest_actual_action_path
