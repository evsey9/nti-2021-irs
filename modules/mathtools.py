import math

# Math related code


class Vector2:
	"""Class for two-dimensional vectors
	"""
	def __init__(self, x=0, y=0):
		self.x = x
		self.y = y

	def __add__(self, other):
		return Vector2(self.x + other.x, self.y + other.y)

	def __sub__(self, other):
		return Vector2(self.x - other.x, self.y - other.y)

	def __mul__(self, scalar):
		return Vector2(self.x * scalar, self.y * scalar)

	def __truediv__(self, scalar):
		return Vector2(self.x / scalar, self.y / scalar)

	def __floordiv__(self, scalar):  # Untested
		return self.normalized() * (self.length() // scalar)

	def __round__(self, n=None):  # Untested
		return Vector2(round(self.x, n), round(self.y, n))

	def __abs__(self):
		return self.length()

	def __bool__(self):
		return self.length() != 0

	def __str__(self):
		return f"({self.x}, {self.y})"

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.x == other.x and self.y == other.y
		else:
			return False

	def to_list(self, sequence="x y"):
		"""Turns vector into a list defined by sequence
		:param str sequence: Sequence that defines the list, string of x and y split by spaces
		:rtype list:
		"""
		sequence_list = sequence.split(" ")
		output_list = []
		for i in sequence_list:
			if i.lower() == "x" or i.lower() == "y":
				output_list.append(self.x if i.lower() == "x" else self.y)
			else:
				raise ValueError("Sequence element is not x or y")
		return output_list

	def length(self):
		"""Returns vector magnitude"""
		return math.sqrt(self.x * self.x + self.y * self.y)

	def normalized(self):
		"""Returns vector pointing in the same direction with length of 1. Returns zero-vector if length is 0."""
		if self.length() != 0:
			return self / self.length()
		else:
			return self

	def dot(self, other):
		"""Returns dot product of self with vector other
		:param Vector2 other:
		"""
		return self.x * other.x + self.y * other.y


def sign(n):
	return 1 if n >= 0 else -1


def intersect_lines(x1, y1, x2, y2, x3, y3, x4, y4):
	"""
	Intersects two lines, (x1, y1) to (x2, y2) and (x3, y3) to (x4, y4)
	:return: Intersection point of two lines if they intersect, otherwise False
	"""

	# Make sure that no lines have a length of 0
	if x1 == x2 and y1 == y2 or x3 == x4 and y3 == y4:
		return False

	denominator = ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1))
	# Make sure that the lines aren't parallel
	if denominator == 0:
		return False

	ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator
	ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator

	# Make sure that intersections are on the lines
	if ua < 0 or ua > 1 or ub < 0 or ub > 1:
		return False

	x = x1 + ua * (x2 - x1)
	y = y1 + ua * (y2 - y1)

	return x, y


def quadrilateral_area(x1, y1, x2, y2, x3, y3, x4, y4):
	"""Finds the area of a quadrangle with points defined as:
	(x1, y1), (x2, y2), (x3, y3), (x4, y4)"""
	return abs(x1 * y2 + x2 * y3 + x3 * y4 + x4 * y1 - x2 * y1 - x3 * y2 - x4 * y3 - x1 * y4) / 2


def map_value_to_range(value, in_min, in_max, out_min, out_max):
	"""Maps a value from range (in_min, in_max) to range (out_min, out_max) For examples, see tests
	:param value: Value to map
	:param in_min: Bottom value of the input value range
	:param in_max: Top value of the input value range
	:param out_min: Bottom value of the output value range
	:param out_max: Top value of the output value range
	:return: Mapped value
	"""
	return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
