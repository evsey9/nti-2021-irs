from enum import Enum
import modules.code_globals as code_globals


# Helper code
def debug_decorator(func):
	"""Decorator that executes function only if globals.DEBUG is True
	:param func: Function to decorate
	:return: Decorated function
	"""
	def wrapper(*args, **kwargs):
		if code_globals.DEBUG:
			func(*args, **kwargs)
	return wrapper


@debug_decorator
def debug_print(*args, **kwargs):
	print(*args, **kwargs)


def nums_to_path(nums):
	"""Converts a list of numbers into a list of robot moves
	:param list[int] nums:
	:rtype: list[str]
	"""
	moves = ["N", "L", "R", "F"]
	return list(map(lambda x: moves[x], nums))


class Dir:
	"""Class for direction numbers"""
	UP = 0
	RIGHT = 1
	DOWN = 2
	LEFT = 3


class DirVec:
	"""Class for direction vectors [x, y]"""
	vectors = [[], [], [], []]
	vectors[Dir.UP] = [0, -1]
	vectors[Dir.RIGHT] = [1, 0]
	vectors[Dir.DOWN] = [0, 1]
	vectors[Dir.LEFT] = [-1, 0]
	UP = vectors[Dir.UP]
	RIGHT = vectors[Dir.RIGHT]
	DOWN = vectors[Dir.DOWN]
	LEFT = vectors[Dir.LEFT]

