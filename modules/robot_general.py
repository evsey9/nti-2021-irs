import modules.robot_globals as robot_globals
import math
import time


# General TRIK code
def get_time():
	"""Gets time since start of program"""
	return time.time() - robot_globals.__interpretation_started_timestamp__
