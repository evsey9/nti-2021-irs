import numpy as np
import cv2

# Color related code

def rgb_hex_array_to_rgb_24(hex_array):
	"""Converts an RGB HEX string array into an rgb24 array
	:param list[str] hex_array: List of strings representing colors coded in HEX
	"""
	return list(map(lambda x: int(x, 16), hex_array))


def rgb24_to_rgb(rgb24):
	"""Converts an RGB24 value into an RGB tuple
	:type rgb24: int
	:return: Tuple of 3 floats, ranging from 0 to 1
	"""
	# return [((rgb24 & int((0xF * (0x100 ** i + 0x100 ** (i + 0.5))))) >> 8 * i) / 255 for i in range(3)[::-1]] one-liner
	rgb_r = ((rgb24 & 0xFF0000) >> 16)
	rgb_g = ((rgb24 & 0x00FF00) >> 8)
	rgb_b = (rgb24 & 0x0000FF)
	return rgb_r, rgb_g, rgb_b


def rgb_to_greyscale(rgb):
	"""Converts an RGB tuple into a greyscale value
	:param tuple[float, float, float] rgb: RGB color tuple
	:rtype: float
	"""
	# Optimal coefficients for RGB to greyscale conversion - all must add up to 1!
	rgb_greyscale_coefficients = (0.299, 0.587, 0.114)
	return sum([rgb[i] * coeff for i, coeff in enumerate(rgb_greyscale_coefficients)])


def rgb_to_hsv(rgb):
	# R, G, B values are divided by 255
	# to change the range from 0..255 to 0..1:
	r, g, b = rgb[0] / 255.0, rgb[1] / 255.0, rgb[2] / 255.0

	# h, s, v = hue, saturation, value
	cmax = max(r, g, b)  # maximum of r, g, b
	cmin = min(r, g, b)  # minimum of r, g, b
	diff = cmax - cmin  # diff of cmax and cmin.
	h = 0
	# if cmax and cmax are equal then h = 0
	if cmax == cmin:
		h = 0

	# if cmax equal r then compute h
	elif cmax == r:
		h = (60 * ((g - b) / diff) + 360) % 360

	# if cmax equal g then compute h
	elif cmax == g:
		h = (60 * ((b - r) / diff) + 120) % 360

	# if cmax equal b then compute h
	elif cmax == b:
		h = (60 * ((r - g) / diff) + 240) % 360

	# if cmax equal zero
	if cmax == 0:
		s = 0
	else:
		s = (diff / cmax) * 100

	# compute v
	v = cmax * 100
	return h, s, v


def rgb24_matrix_to_grey_matrix(matrix):
	"""Converts an RGB24 2D matrix to a greyscale matrix
	:param list[list[int]] matrix: 2D color matrix of RGB24 values
	"""
	rgb_matrix = [[rgb24_to_rgb(j) for j in i] for i in matrix]
	greyscale_matrix = [[rgb_to_greyscale(j) for j in i] for i in rgb_matrix]
	return greyscale_matrix


def rgb_matrix_to_grey_matrix(matrix):
	"""Converts an RGB 2D matrix to a greyscale matrix
	:param matrix: 2D color matrix of RGB tuples
	"""
	greyscale_matrix = cv2.cvtColor(matrix, cv2.COLOR_RGB2GRAY)
	return greyscale_matrix


def greyscale_to_binary(greyscale_value, threshold_grey):
	"""
	Converts a greyscale color value to 1 if it's higher than threshold_grey, 0 otherwise.
	:param threshold_grey: Recommended 32
	:type greyscale_value: float
	:type threshold_grey: float
	"""
	return 1 if greyscale_value > threshold_grey else 0


def grey_matrix_to_mask_matrix(matrix):
	"""Converts a greyscale 2D matrix into a binary matrix"""
	threshold_grey = 10  # If greyscale value is higher than this threshold, then the color is white, otherwise black
	_, mask_matrix = cv2.threshold(matrix, threshold_grey, 1, cv2.THRESH_BINARY)
	return mask_matrix


def filter_color_mask(color, color_min, color_max):
	"""Returns 1 if color is in bounds and 0 otherwise
	:param color: Color to check
	:param color_min: Color minimum
	:param color_max: Color maximum
	:return: 1 or 0
	"""
	color_flag = True
	for colori, cmini, cmaxi in zip(color, color_min, color_max):
		if colori < cmini or colori > cmaxi:
			color_flag = False
			break
	return 1 if color_flag else 0
