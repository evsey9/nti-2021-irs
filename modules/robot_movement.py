import modules.robot_general as robot_general
import modules.robot_globals as robot_globals
import modules.code_globals as code_globals
import modules.helpers as helpers
import modules.mathtools as mathtools
import modules.robot_laser as laser
import math

from modules.robot_globals import robot_object as robot

# Movement calculation functions.

def dist_to_cpr(dist, robot_cpr, robot_d):
	"""Converts distance to travel into encoder counts. Dist and wheel diameter must be the same unit of measurement
	:param dist: Distance
	:param robot_cpr: Encoder counts per revolution of the robot
	:param robot_d: Wheel diameter of the robot
	:return: Encoder counts
	"""
	return (dist / (math.pi * robot_d)) * robot_cpr


def calculate_delta_angle(ang1, ang2):
	"""Calculates the difference between two angles
	:param ang1: First angle
	:param ang2: Second angle
	:return: Difference between two angles, if positive - need to turn right, if negative - need to turn left
	"""
	if ang1 >= 0 and ang2 >= 0 or ang1 <= 0 and ang2 <= 0:
		delta_angle = ang2 - ang1
	elif ang1 >= 0 and ang2 <= 0:
		delta_angle = -1 * (ang1 - ang2) if ang1 - ang2 <= 180 else 360 - ang1 + ang2
	elif ang1 <= 0 and ang2 >= 0:
		delta_angle = abs(ang1) + ang2 if abs(ang1) + ang2 <= 180 else -1 * (360 - abs(ang1) - ang2)
	
	if abs(delta_angle) >= 360:
		delta_angle -= 360 * mathtools.sign(delta_angle)
	
	return delta_angle


def calculate_accelerating_motor_speed(cur_percentage, v_min, v_max, accel_start, accel_end, decel_start, decel_end):
	"""Calculates the current motor speed with given acceleration/deceleration start and end values
	:param cur_percentage: Percentage of how much of the path has the robot travelled
	:param v_min: Starting speed
	:param v_max: Speed after accelerating
	:param accel_start: Percentage at which to begin accelerating
	:param accel_end: Percentage at which to stop accelerating
	:param decel_start: Percentage at which to begin decelerating
	:param decel_end: Percentage at which to stop decelerating
	:return: Motor speed
	"""
	if cur_percentage < accel_start or cur_percentage >= decel_end:
		return v_min
	elif accel_start <= cur_percentage < accel_end:
		return mathtools.map_value_to_range(cur_percentage, accel_start, accel_end, v_min, v_max)
	elif accel_end <= cur_percentage < decel_start:
		return v_max
	elif decel_start <= cur_percentage < decel_end:
		return mathtools.map_value_to_range(cur_percentage, decel_start, decel_end, v_max, v_min)

# Robot movement code




def get_encoder_readings():
	"""Gets encoder readings for left and right encoder"""
	encoder_readings = robot.getEncoders()
	return encoder_readings["right"], encoder_readings["left"]  # Robot model wheels are placed incorrectly for some reason



def get_yaw(read_gyro_func=robot.getDirection):
	"""Gets global yaw of the robot in degrees (yaw increases clockwise)
	:param read_gyro_func: Function that reads yaw from the gyroscope
	:return: True yaw value in degrees from -180 to 180
	"""
	yaw_value = -math.degrees(read_gyro_func())
	if yaw_value > 180:
		yaw_value = yaw_value - 360
	if yaw_value < -180:
		yaw_value = yaw_value + 360
	return yaw_value


def set_speed(linear_speed, angular_speed=0):
	"""Sets the speeds of the robot
	:param linear_speed: Desired linear speed of robot in meters per seconds
	:param angular_speed: Desired angular speed of robot in radians per seconds
	"""
	linear_speed, angular_speed = min(max(linear_speed, -robot_globals.robot_const["max_v"]), robot_globals.robot_const["max_v"]), min(max(angular_speed, -robot_globals.robot_const["max_turn_v"]), robot_globals.robot_const["max_v"])
	robot.setVelosities(linear_speed, angular_speed)


def stop_motors(back_stop=False):
	"""Stops the motors immediately"""
	# if back_stop:
	# 	set_speed(-1)
	# 	robot.sleep(0.00)
	set_speed(0)


def turn_by_encoder(angle, motor_slowstart=False):
	"""Turns the robot by desired angle using encoders
	:param angle: How much to turn, measured in degrees
	"""
	angle_sign = mathtools.sign(angle)
	enc_n = lambda: get_encoder_readings()[0] if angle_sign == 1 else lambda: get_encoder_readings()[1]
	start_enc_value = enc_n()
	total_path = (robot_globals.robot_dimensions["track"] * abs(angle)) / (robot_globals.robot_dimensions["wheel_diameter"] * 360) * robot_globals.robot_const["cpr"]
	if motor_slowstart:
		set_speed(0, angle_sign * robot_globals.robot_const["v_min"])
		robot.sleep(0.015)
	set_speed(0, angle_sign * robot_globals.robot_const["turn_v"])
	while enc_n() - start_enc_value < total_path:
		robot.sleep(0.015)
	set_speed(0, 0)


def turn_by_gyro(angle, accel_period=25):
	"""Turns the robot to desired angle using the gyroscope
	:param angle: Angle to which to turn, measured in degrees
	:param accel_period: How much of the way to decelerate, in percent
	"""
	if angle > 180:
		angle -= 360
	starting_yaw = get_yaw()
	base_delta = calculate_delta_angle(starting_yaw, angle)
	sign = mathtools.sign(base_delta)
	turn_speed = robot_globals.robot_const["turn_v"]
	set_speed(0, sign * turn_speed)
	while abs(calculate_delta_angle(get_yaw(), angle)) > 3.0 and mathtools.sign(calculate_delta_angle(get_yaw(), angle)) == sign:
		angle_left = abs(calculate_delta_angle(get_yaw(), angle))
		percentage = max(100 - angle_left / abs(base_delta) * 100, 0)
		# print(percentage)
		current_turn_speed = calculate_accelerating_motor_speed(percentage, robot_globals.robot_const["turn_v_min"], robot_globals.robot_const["turn_v"], 0, 0, 100 - accel_period, 100)
		set_speed(0, sign * current_turn_speed)
		robot.sleep(0.015)
	set_speed(0, 0)


def calculate_deviation_from_centre(alignment="adaptive", distance_to_wall=robot_globals.consts['cell_size'] / 2):
	"""Calculates the robot's deviation from the centre of the cell using gyroscope data and distance sensor data
	:param str alignment: How to align the robot. "left" - left alignment, "right" - right alignment, "adaptive" - adaptive alignment
	:return: Deviation of the robot from the centre (positive - robot is to the right, negative - robot is to the left)
	"""
	cell_threshold = distance_to_wall * 1.5
	laser_reading = laser.LaserReading()
	sensor_left_reading_max = laser_reading.get_func_reading_from_range(-105, -75, max)
	sensor_left_reading = laser_reading.get_reading_from_angle(-90)
	sensor_right_reading_max = laser_reading.get_func_reading_from_range(75, 105, max)
	sensor_right_reading = laser_reading.get_reading_from_angle(90)
	gyro_reading = get_yaw()
	left_distance = -1 * (distance_to_wall - sensor_left_reading * math.cos(math.radians(abs(calculate_delta_angle(robot_globals.angles[robot_globals.robot_var["azimuth"]], gyro_reading)))))
	right_distance = distance_to_wall - sensor_right_reading * math.cos(math.radians(abs(calculate_delta_angle(robot_globals.angles[robot_globals.robot_var["azimuth"]], gyro_reading))))
	
	if alignment == "adaptive":
		if abs(left_distance) < abs(right_distance):
			min_distance = left_distance
			max_distance = sensor_left_reading_max[0]
		else:
			min_distance = right_distance
			max_distance = sensor_right_reading_max[0]
		if max_distance > cell_threshold:
			return 0
		else:
			return min_distance
	elif alignment == "left":
		if sensor_left_reading_max > cell_threshold:
			return 0
		else:
			return left_distance
	else:
		if sensor_right_reading_max > cell_threshold:
			return 0
		else:
			return right_distance

# Functions for moving straight. Error functions all give positive value if robot is veering right and negative if left


def get_encoder_error(enc_l_value, enc_r_value, enc_l_start, enc_r_start):
	"""Gets difference between amount of encoder steps detected since start of path
	:param enc_l_value: Current left encoder value
	:param enc_r_value: Current right encoder value
	:param enc_l_start: Starting value of left encoder
	:param enc_r_start: Starting value of right encoder
	:return: Difference between encoder steps taken since enc_l_start and enc_r_start by the encoders
	"""
	return (enc_l_value - enc_l_start) - (enc_r_value - enc_r_start)


def get_gyroscope_error(gyro_value, gyro_straight):
	"""Gets the difference between current gyroscope yaw value and desired gyroscope yaw
	:param gyro_value: Current gyroscope yaw reading
	:param gyro_straight: Desired gyroscope yaw
	:return: Angle difference
	"""
	return calculate_delta_angle(gyro_straight, gyro_value)


def move_straight(length, v_min=robot_globals.robot_const["v_min"], v_max=robot_globals.robot_const["v"], accel_period=100, enc_align=False, gyro_align=True, wall_align=False, gyro_angle=None, distance_to_wall=robot_globals.consts['cell_size'] / 2, wall_align_type="adaptive", kpd_alt=False, func=None, func_period=robot_globals.consts['cell_size'], update_coords=False):
	"""Moves the robot in a straight line
	:param length: How much to move the robot (in meters)
	:param v_min: Starting speed of the robot
	:param v_max: Maximal speed of the robot
	:param accel_period: For how much of the way to accelerate, in percent
	:param bool enc_align: Whether to align using the encoders or not
	:param bool gyro_align: Whether to align using the gyroscope or not
	:param bool wall_align: Whether to align using the walls (distance sensor) or not
	:param gyro_angle: The angle to align to when using gyroscope align
	:param distance_to_wall: The distance from the wall at which to keep the robot when using wall alignment
	:param wall_align_type: String used for determining wall alignment type. Should be either adaptive, right or left. (closest wall, right wall or left wall)
	:param kpd_alt: Whether to use the set of alternative P regulator values
	:param func: The function to call every func_period meters when driving
	:param func_period: The distance the robot should should move before calling func()
	:param update_coords: Whether to update the coordinates of the robot during driving
	"""
	if gyro_angle is None and gyro_align:
		gyro_angle = robot_globals.angles[robot_globals.robot_var["azimuth"]]
	alignment = [enc_align, gyro_align, wall_align]
	k = sum(alignment)
	sign = mathtools.sign(length)
	length = abs(length)
	encoder_path = dist_to_cpr(length, robot_globals.robot_const["cpr"], robot_globals.robot_dimensions["wheel_diameter"])
	encoder_start = get_encoder_readings()

	# TODO: find what set of KP values works best
	enc_kP_k_not_1 = 0.65 if kpd_alt else 0.1
	gyro_kP_k_not_1 = 1.25 if kpd_alt else 1.2
	wall_kP_k_not_1 = 1.25 if kpd_alt else 4

	enc_kP_k_equals_1 = 0.65 if kpd_alt else 0.65
	gyro_kP_k_equals_1 = 1.25 if kpd_alt else 1.25
	wall_kP_k_equals_1 = 1.25 if kpd_alt else 1.25

	enc_kP = enc_kP_k_equals_1 if k == 1 else enc_kP_k_not_1
	gyro_kP = gyro_kP_k_equals_1 if k == 1 else gyro_kP_k_not_1
	wall_kP = wall_kP_k_equals_1 if k == 1 else wall_kP_k_not_1

	control_array = [0, 0, 0]

	encoder_readings = get_encoder_readings()
	encoder_steps = [encoder_readings[0] - encoder_start[0], encoder_readings[1] - encoder_start[1]]
	encoder_steps_average = sum(encoder_steps) / 2

	func_enc_period = dist_to_cpr(func_period, robot_globals.robot_const["cpr"], robot_globals.robot_dimensions["wheel_diameter"])
	func_call_index = 1

	cell_enc_period = dist_to_cpr(robot_globals.consts["cell_size"], robot_globals.robot_const["cpr"], robot_globals.robot_dimensions["wheel_diameter"])
	cell_update_index = 1
	while encoder_steps_average * sign < encoder_path:
		encoder_readings = get_encoder_readings()
		encoder_steps = [encoder_readings[0] - encoder_start[0], encoder_readings[1] - encoder_start[1]]
		encoder_steps_average = sum(encoder_steps) / 2
		path_percentage = abs(encoder_steps_average) / encoder_path * 100
		base_speed = calculate_accelerating_motor_speed(path_percentage, v_min, v_max, 0, 0,
														100 - accel_period, 100)
		if enc_align:
			encoder_error = get_encoder_error(encoder_steps[0], 0, encoder_steps[1], 0)
			encoder_control = encoder_error * enc_kP * base_speed / 100
			if not kpd_alt:
				encoder_control /= k
			control_array[0] = encoder_control

		if gyro_align:
			gyroscope_readings = get_yaw()
			gyroscope_error = get_gyroscope_error(gyroscope_readings, gyro_angle)
			gyroscope_control = gyroscope_error * gyro_kP
			control_array[1] = gyroscope_control

		if wall_align:
			deviation_from_centre = calculate_deviation_from_centre(wall_align_type, distance_to_wall)
			wall_error = deviation_from_centre
			wall_control = wall_error * wall_kP / k * 75
			control_array[2] = wall_control

		if not kpd_alt and k != 1 and control_array[2] == 0:
			control_array[0] /= 2
			control_array[1] *= 2

		if update_coords and encoder_steps_average >= cell_enc_period * cell_update_index:
			vec = helpers.DirVec.vectors[robot_globals.robot_var["azimuth"]]
			robot_globals.robot_var["x"] += vec[0]
			robot_globals.robot_var["y"] += vec[1]
			cell_update_index += 1
		
		if encoder_steps_average >= func_enc_period * func_call_index:
			if func is not None:
				func()
			func_call_index += 1
			# print("func call", func_call_index)

		set_speed(base_speed * sign, -sum(control_array) * sign * (base_speed / v_max) / 30)
		robot.sleep(0.02)
	stop_motors(True)

# Functions that work with coordinates


def place_turn_to(azimuth, go_forward=False):
	"""Turns the robot in place to an azimuth
	:param azimuth: Azimuth to turn to
	:param bool go_forward: Whether to go half of the robot's track forward or not
	"""
	if go_forward:
		move_straight(robot_globals.robot_dimensions["track"] / 2)
	turn_by_gyro(robot_globals.angles[azimuth])
	robot_globals.robot_var["azimuth"] = azimuth
	if go_forward:
		move_straight(-robot_globals.robot_dimensions["track"] / 2)


def place_turn_right(go_forward=False):
	"""Turns the robot in place clockwise 90 degrees"""
	new_azimuth = (robot_globals.robot_var["azimuth"] + 1) % 4
	if go_forward:
		move_straight(robot_globals.robot_dimensions["track"] / 2)
	turn_by_gyro(robot_globals.angles[new_azimuth])
	robot_globals.robot_var["azimuth"] = new_azimuth
	if go_forward:
		move_straight(-robot_globals.robot_dimensions["track"] / 2)


def place_turn_left(go_forward=False):
	"""Turns the robot in place counter-clockwise 90 degrees"""
	new_azimuth = (robot_globals.robot_var["azimuth"] - 1) % 4
	if go_forward:
		move_straight(robot_globals.robot_dimensions["track"] / 2)
	turn_by_gyro(robot_globals.angles[new_azimuth])
	robot_globals.robot_var["azimuth"] = new_azimuth
	if go_forward:
		move_straight(-robot_globals.robot_dimensions["track"] / 2)


def move_cells(cells, enc_align=False, gyro_align=True, wall_align=True, distance_to_wall=robot_globals.consts['cell_size'] / 2, wall_align_type="adaptive", accel_period=100, cell_func=None, func_cell_period=1):
	"""Moves the robot forward by cells
	:param cells: How many cells to move forward
	:param bool enc_align: Whether to align using the encoders or not
	:param bool gyro_align: Whether to align using the gyroscope or not
	:param bool wall_align: Whether to align using the walls (distance sensor) or not
	:param distance_to_wall: The distance from the wall at which to keep the robot when using wall alignment
	:param wall_align_type: String used for determining wall alignment type. Should be either adaptive, right or left. (closest wall, right wall or left wall)
	:param accel_period: For how much of the way to accelerate during the last cell, in percent
	:param cell_func: The function to call every func_cell_period cells while driving
	:param func_cell_period: The number of cells the robot should should move before calling cell_func()
	"""
	move_straight(cells * robot_globals.consts["cell_size"], accel_period=accel_period / max(cells, 1), enc_align=enc_align, gyro_align=gyro_align, wall_align=wall_align, wall_align_type=wall_align_type, distance_to_wall=distance_to_wall, func=cell_func, func_period=func_cell_period * robot_globals.consts['cell_size'] * func_cell_period, update_coords=True)


def go_actions(actions, cell_func=None):
	i = 0
	while i < len(actions):
		action = actions[i]
		if action == "F":
			forward_count = 1
			while i + forward_count < len(actions) and actions[i + forward_count] == "F":
				forward_count += 1
			move_cells(forward_count, cell_func=cell_func)
			i += forward_count
		elif action == "L":
			place_turn_left()
			i += 1
		elif action == "R":
			place_turn_right()
			i += 1
		else:
			raise ValueError("Incorrect action in list")