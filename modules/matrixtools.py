import itertools


# Matrix related code

def list_to_matrix(input_list, matrix_shape):
	"""Turns a list into a 2D matrix defined by matrix_shape
	:type input_list: list
	:type matrix_shape: tuple[int, int]
	:rtype: list[list]
	"""
	return [[input_list[i * matrix_shape[1] + j] for j in range(matrix_shape[1])] for i in range(matrix_shape[0])]


def matrix_to_list(matrix):
	"""Flattens matrix into a 1d list"""
	return itertools.chain.from_iterable(matrix)


def rotate_matrix_90_degrees_clockwise(matrix):
	"""Rotates matrix 90 degrees clockwise
	:type matrix: list[list]
	"""
	return [[matrix[-1 - j][i] for j in range(len(matrix))] for i in range(len(matrix[0]))]


def get_90_deg_cw_rotated_matrix_index(matrix_shape, matrix_index):
	"""Gets the new index of an element at index matrix_index in a 2D matrix rotated 90 degrees clockwise
	:param tuple[int, int] matrix_shape: Matrix shape of matrix with elem at matrix_index
	:param tuple[int, int] matrix_index: Index of element in matrix
	:return: New index of element in rotated matrix
	"""
	y_len = matrix_shape[0]
	x_len = matrix_shape[1]
	y, x = matrix_index[0] - y_len / 2.0 + 0.5, matrix_index[1] - x_len / 2.0 + 0.5  # Getting position relatively centre
	y, x = x, -y  # Rotating coordinate clockwise
	y, x = y + x_len / 2.0 - 0.5, x + y_len / 2.0 - 0.5  # Don't question it
	return int(y), int(x)


def matrix_map(func, matrix, *funcargs, arg_matrix=False):
	"""Applies a function on each element of a matrix
	:param func: Function to apply on each matrix element
	:param list[list] matrix: Input matrix
	:param funcargs: Additional arguments to pass to function
	:param bool arg_matrix: Apply funcargs[i][j] arguments onto matrix[i][j] or not
	:rtype: list[list]:
	"""
	if arg_matrix:
		return [[func(j, *funcargs[0][_i][_j], *funcargs[1:]) for _j, j in enumerate(i)] for _i, i in enumerate(matrix)]
	else:
		return [[func(j, *funcargs) for j in i] for i in matrix]


def check_if_matrix_has_true_element(matrix):
	"""Checks if a 2D matrix has any true elements.
	:param matrix: 2D matrix of elements
	:return: true if matrix has any true elements, false otherwise
	"""
	for row in matrix:
		for elem in row:
			if elem:
				return True
	return False
